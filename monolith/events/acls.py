from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import json
import requests



def get_photo(city, state):
    url = "https://api.pexels.com/v1/search"
    headers = {
        "Authorization": PEXELS_API_KEY
    }

    payload = {
        "query": f"{city} {state}",
        "per_page": 1,
    }

    response = requests.get(url, params=payload, headers=headers)
    photo_dict = json.loads(response.content)
    try:
        return photo_dict["photos"][0]["url"]
    except (KeyError, IndexError):
        return None


def get_weather_data(city, state):
    url = "http://api.openweathermap.org/geo/1.0/direct"

    payload = {
        "q": f"{city},{state},US",
        "appid": OPEN_WEATHER_API_KEY
    }

    geocode_response = requests.get(url, params=payload)
    geocode_data = geocode_response.json()

    lat = geocode_data[0]["lat"]
    lon = geocode_data[0]["lon"]

    geocode_url = f"https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&appid={OPEN_WEATHER_API_KEY}"
    weather_response = requests.get(geocode_url)
    weather_data = weather_response.json()

    temperature = weather_data["main"]["temp"]
    description = weather_data["weather"][0]["description"]
    return temperature, description
